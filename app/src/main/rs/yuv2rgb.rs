#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

int imageWidth;
int imageHeight;
rs_allocation yuvData;

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float r,g,b,y1,u,v;
    int index = y*imageWidth + x;
    int size = imageWidth*imageHeight;
    int otherIndex = ((y/2)*(imageWidth/2) + (x/2))*2 + size;


    uchar *temp = (uchar *)rsGetElementAt(yuvData,index);
    y1 = *temp/255.0f;
    temp = (uchar *)rsGetElementAt(yuvData,otherIndex+1);
    u = *temp/255.0f;
    temp = (uchar *)rsGetElementAt(yuvData,otherIndex);
    v = *temp/255.0f;

    u -= 0.5f;
    v -= 0.5f;
    y1=1.1643*(y-0.0625);
    r=y1+1.5958*v;
    g=y1-0.39173*u-0.81290*v;
    b=y1+2.017*u;

    float3 color = {r,g,b};
    color = clamp(color,0.0f,1.0f);
    *v_out = rsPackColorTo8888(color.r,color.g,color.b,1.0f);
}

void convert(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}