#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

rs_allocation inTexture;
rs_allocation matrixTexture;

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 apixel = rsUnpackColor8888(*v_in);
    *v_out = rsPackColorTo8888(apixel);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}