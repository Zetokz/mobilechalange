#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

float gammaValue;

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 apixel = rsUnpackColor8888(*v_in);
    float3 color = apixel.rgb;
    float3 gamma = {gammaValue,gammaValue,gammaValue};
    float3 newColor = pow(color, gamma);
    *v_out = rsPackColorTo8888(newColor.r,newColor.g,newColor.b,apixel.a);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}