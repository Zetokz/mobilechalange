#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

float brightnessValue;


void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 apixel = rsUnpackColor8888(*v_in);
    float3 pixel = apixel.rgb;
    float factor = brightnessValue;
    pixel = pixel + factor;
    pixel = clamp(pixel,0.0f,1.0f);
    *v_out = rsPackColorTo8888(pixel.r,pixel.g,pixel.b,apixel.a);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}