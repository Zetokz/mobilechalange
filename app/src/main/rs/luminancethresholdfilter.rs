#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

float thresholdValue;
const static float3 luminanceWeighting = {0.2125, 0.7154, 0.0721};

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 pixel = rsUnpackColor8888(*v_in);
    float luminance = dot(pixel.rgb, luminanceWeighting);
    float newValue = step(thresholdValue,luminance);
    float3 color = {newValue,newValue,newValue};
    *v_out = rsPackColorTo8888(color.r,color.g,color.b,pixel.a);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}