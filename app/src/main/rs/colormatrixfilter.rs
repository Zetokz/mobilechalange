#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

float intensityValue;
rs_matrix4x4 colorMatrix;

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 pixel = rsUnpackColor8888(*v_in);
    float4 color = rsMatrixMultiply(&colorMatrix,pixel);
    color = (intensityValue * color) + ((1.0f - intensityValue) * pixel);
    color = clamp(color,0.0f,1.0f);
    *v_out = rsPackColorTo8888(color);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}