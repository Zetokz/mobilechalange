#pragma version(1)
#pragma rs java_package_name(ua.com.challenge.filters.scripts)

rs_allocation toneTexture;

void root(const uchar4 *v_in, uchar4 *v_out,const void *userData, uint32_t x, uint32_t y) {
    float4 apixel = rsUnpackColor8888(*v_in);
    float3 pixel = apixel.rgb;

    uchar4 *element = (uchar4 *)rsGetElementAt(toneTexture, floor(pixel.r*255.0f));
    float Rpixel = rsUnpackColor8888(*element).r;

    element = (uchar4 *)rsGetElementAt(toneTexture, floor(pixel.g*255.0f));
    float Gpixel = rsUnpackColor8888(*element).g;

    element = (uchar4 *)rsGetElementAt(toneTexture, floor(pixel.b*255.0f));
    float Bpixel = rsUnpackColor8888(*element).b;

    *v_out = rsPackColorTo8888(Rpixel,Gpixel,Bpixel,apixel.a);
}

void filter(rs_script script,rs_allocation inAllocation,rs_allocation outAllocation){
    rsForEach(script, inAllocation, outAllocation, 0, 0);
}