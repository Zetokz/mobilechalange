package ua.com.challenge.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;

import ua.com.challenge.effects.FilterSystem;

/**
 * Created by podo on 28.11.15.
 */
public class FilterUtils {
    private Context context;
    private RenderScript mRs;
    Allocation mInAllocation;
    Allocation mOutAllocation;
    Bitmap originalBitmap;
    Bitmap resultBitmap;


    public FilterUtils(Context ctx, Bitmap originalBitmap) {
        this.context = ctx;
        this.originalBitmap = originalBitmap;
        resultBitmap = originalBitmap;
//        resultBitmap = Bitmap.createBitmap(this.originalBitmap.getWidth(),this.originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        mRs = RenderScript.create(context);
        mInAllocation = Allocation.createFromBitmap(mRs, this.originalBitmap,
                Allocation.MipmapControl.MIPMAP_ON_SYNC_TO_TEXTURE,
                Allocation.USAGE_SCRIPT);
        mOutAllocation = Allocation.createTyped(mRs, mInAllocation.getType());
        mOutAllocation.copyFrom(this.originalBitmap);
        mOutAllocation.copyTo(resultBitmap);
    }

    public void applyFilter(String currentEffect, float value) {
        FilterSystem.applyFilter(mInAllocation, mOutAllocation, value, currentEffect, mRs, context.getResources(), resultBitmap);
        mOutAllocation.copyTo(resultBitmap);
    }

    public Bitmap getResultBitmap() {
        return resultBitmap;
    }

    public void setOriginalBitmap(Bitmap originalBitmap) {
        this.originalBitmap = originalBitmap;
        mInAllocation = Allocation.createFromBitmap(mRs, this.originalBitmap,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        mOutAllocation = Allocation.createTyped(mRs, mInAllocation.getType());
        mOutAllocation.copyFrom(this.originalBitmap);
        mOutAllocation.copyTo(resultBitmap);
    }
}
