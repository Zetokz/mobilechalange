package ua.com.challenge.utils;

import com.isseiaoki.simplecropview.CropImageView;

public class CropModeHelper {

    public static void aspect1_1(CropImageView _cropImageView){
        _cropImageView.setCropMode(CropImageView.CropMode.RATIO_1_1);
    }

    public static void aspect3_4(CropImageView _cropImageView){
        _cropImageView.setCropMode(CropImageView.CropMode.RATIO_3_4);
    }

    public static void aspect9_16(CropImageView cropImageView){
        cropImageView.setCropMode(CropImageView.CropMode.RATIO_9_16);
    }

    public static void fitimage(CropImageView _cropImageView){
        _cropImageView.setCropMode(CropImageView.CropMode.RATIO_FIT_IMAGE);
    }

    public static void ratioFree(CropImageView _cropImageView){
        _cropImageView.setCropMode(CropImageView.CropMode.RATIO_FREE);
    }

    public static void circle(CropImageView _cropImageView){
        _cropImageView.setCropMode(CropImageView.CropMode.CIRCLE);
    }
}
