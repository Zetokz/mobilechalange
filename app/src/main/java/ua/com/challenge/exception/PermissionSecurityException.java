package ua.com.challenge.exception;

import android.support.annotation.NonNull;

public class PermissionSecurityException extends Throwable {
    private final String[] mRequiredPermissions;

    public PermissionSecurityException(String[] _requiredPermissions, String _message) {
        super(_message);
        mRequiredPermissions = _requiredPermissions == null ? new String[]{} : _requiredPermissions;
    }

    @NonNull
    public String[] getRequiredPermissions() {
        return mRequiredPermissions;
    }
}
