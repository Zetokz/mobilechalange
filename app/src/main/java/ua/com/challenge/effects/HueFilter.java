package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.RenderScript;

import ua.com.challenge.filters.scripts.ScriptC_huefilter;

public class HueFilter extends ScriptC_huefilter {
    public HueFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    public void setHue(float newHue) {
        newHue = (float) ((newHue % 360.0f) * Math.PI / 180.0f);
        super.set_hueAdjust(newHue);
    }
}
