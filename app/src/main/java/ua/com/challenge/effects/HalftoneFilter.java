package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.RenderScript;

import ua.com.challenge.filters.scripts.ScriptC_halftonefilter;

public class HalftoneFilter extends ScriptC_halftonefilter {
    float mWidth = 0.0f;

    public HalftoneFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
    }

    @Override
    public void set_fractionalWidthOfAPixel(float newValue) {
        float singlePixelSpacing;
        if (mWidth != 0.0f)
        {
            singlePixelSpacing = 1.0f / mWidth;
        }
        else
        {
            singlePixelSpacing = 1.0f / 2048.0f;
        }

        if (newValue < singlePixelSpacing)
        {
            newValue = singlePixelSpacing;
        }
        super.set_fractionalWidthOfAPixel(newValue);
    }

    public void setInputSize(int width, int height) {
        mWidth = (float)width;
        float mHeight = (float)height;
        set_aspectRatio(mWidth/mHeight);
    }
}
