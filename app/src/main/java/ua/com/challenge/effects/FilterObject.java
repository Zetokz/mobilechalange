package ua.com.challenge.effects;


public class FilterObject {
    private String value;
    private float defaultValue;
    private float minusValue;
    private float maxValue;
    private float divisorValue;
    private String name;

    public FilterObject(String name, String value, int defaultValue, int maxValue, int minusValue, int divisorValue) {
        this.value = value;
        this.name = name;
        this.defaultValue = defaultValue;
        this.maxValue = maxValue;
        this.minusValue = minusValue;
        this.divisorValue = divisorValue;

    }

    public String getValue() {
        return value;
    }

    public float getDefaultValue() {
        return defaultValue;
    }

    public float getMinusValue() {
        return minusValue;
    }

    public float getMaxValue() {
        return maxValue;
    }

    public float getDivisorValue() {
        return divisorValue;
    }

    public String getName() {
        return name;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
