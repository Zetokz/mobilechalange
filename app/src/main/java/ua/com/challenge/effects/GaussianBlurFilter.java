package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Float2;
import android.renderscript.RenderScript;
import android.renderscript.Script;
import ua.com.challenge.filters.scripts.ScriptC_convolutionseperablefilter;

public class GaussianBlurFilter extends ScriptC_convolutionseperablefilter {
    float blurSize = 2.0f;
    float kernel[];
    Allocation kernelAllocation;

    public GaussianBlurFilter(RenderScript rs) {
        super(rs);
        kernel = makeGaussiannKernel(4);
        kernelAllocation = Allocation.createSized(rs, Element.F32(rs), 9);
        kernelAllocation.copyFrom(kernel);
    }

    public GaussianBlurFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        kernel = makeGaussiannKernel(4);
        kernelAllocation = Allocation.createSized(rs, Element.F32(rs), 9);
        kernelAllocation.copyFrom(kernel);
    }

    @Override
    public void invoke_filter(Script script, Allocation mInAllocation, Allocation mOutAllocation) {
        set_inTexture(mInAllocation);
        set_matrixLenght(9);
        set_XYOffset(new Float2(1 * blurSize, 0));
        set_matrixTexture(kernelAllocation);
        super.invoke_filter(script, mInAllocation, mOutAllocation);
        set_inTexture(mOutAllocation);
        set_XYOffset(new Float2(0, 1 * blurSize));
        super.invoke_filter(script, mInAllocation, mOutAllocation);
    }

    public void setBlurSize(float value) {
        blurSize = value;
    }

    public float[] makeGaussiannKernel(int radius) {
        int r = (int) Math.ceil(radius);
        int rows = r * 2 + 1;
        float[] matrix = new float[rows];
        float sigma = ((float) radius) / 3.0f;
        float sigma22 = 2.0f * sigma * sigma;
        float sigmaPi2 = 2.0f * (float) (Math.PI * sigma);
        float sqrtSigmaPi2 = (float) Math.sqrt(sigmaPi2);
        float radius2 = radius * radius;
        float total = 0.0f;
        int index = 0;
        for (int row = -r; row <= r; row++) {
            float distance = row * row;
            if (distance > radius2)
                matrix[index] = 0.0f;
            else
                matrix[index] = (float) Math.exp(-(distance) / sigma22) / sqrtSigmaPi2;
            total += matrix[index];
            index++;
        }
        for (int i = 0; i < rows; i++)
            matrix[i] /= total;

        return matrix;
    }
}
