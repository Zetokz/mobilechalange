package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.Allocation;
import android.renderscript.Float2;
import android.renderscript.RenderScript;
import android.renderscript.Script;

import ua.com.challenge.R;
import ua.com.challenge.filters.scripts.ScriptC_selectivefilter;

public class SelectiveGaussianBlurFilter extends GaussianBlurFilter {

    ScriptC_selectivefilter mScript;
    public SelectiveGaussianBlurFilter(RenderScript rs) {
        super(rs);
        mScript = new ScriptC_selectivefilter(rs);
        init();
    }

    public SelectiveGaussianBlurFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mScript = new ScriptC_selectivefilter(rs, resources, R.raw.selectivefilter);
        init();
    }

    public void init(){

        setBlurSize(2.0f);
        mScript.set_aspectRatio(0.75f);
        mScript.set_excludeBlurSize(30.0f/320.0f);
        mScript.set_excludeCircleRadius(60.0f/320.0f);
        mScript.set_excludeCirclePoint(new Float2(0.5f, 0.5f));
    }


    public void set_imageWidth(int width){
        super.set_imageWidth(width);
        mScript.set_imageWidth(width);
    }

    public void set_imageHeight(int height){
        super.set_imageHeight(height);
        mScript.set_imageHeight(height);
    }

    @Override
    public void invoke_filter(Script script,Allocation mInAllocation, Allocation mOutAllocation) {
        super.invoke_filter(script, mInAllocation, mOutAllocation);
        mScript.set_blurTexture(mOutAllocation);
        mScript.set_sharpTexture(mInAllocation);
        mScript.invoke_filter(mScript,mInAllocation, mOutAllocation);
    }

    public void set_excludeCirclePoint(Float2 ratio){
        mScript.set_excludeCirclePoint(ratio);
    }
    public void set_excludeCircleRadius(float ratio){
        mScript.set_excludeCircleRadius(ratio);
    }
    public void set_excludeBlurSize(float ratio){
        mScript.set_excludeBlurSize(ratio);
    }
    public void set_aspectRatio(float ratio){
        mScript.set_aspectRatio(ratio);
    }
}
