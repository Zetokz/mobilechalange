package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.Matrix4f;
import android.renderscript.RenderScript;

import ua.com.challenge.filters.scripts.ScriptC_colormatrixfilter;


public class ColorMatrixFilter extends ScriptC_colormatrixfilter {
    public ColorMatrixFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        set_intensityValue(1.0f);
        Matrix4f colorMatrix = new Matrix4f();
        colorMatrix.set(0,0,1.0f);
        colorMatrix.set(1,0,0.0f);
        colorMatrix.set(2,0,0.0f);
        colorMatrix.set(3,0,0.0f);

        colorMatrix.set(0,1,0.0f);
        colorMatrix.set(1,1,1.0f);
        colorMatrix.set(2,1,0.0f);
        colorMatrix.set(3,1,0.0f);

        colorMatrix.set(0,2,0.0f);
        colorMatrix.set(1,2,0.0f);
        colorMatrix.set(2,2,1.0f);
        colorMatrix.set(3,2,0.0f);

        colorMatrix.set(0,3,0.0f);
        colorMatrix.set(1,3,0.0f);
        colorMatrix.set(2,3,0.0f);
        colorMatrix.set(3,3,1.0f);

        set_colorMatrix(colorMatrix);
    }
}
