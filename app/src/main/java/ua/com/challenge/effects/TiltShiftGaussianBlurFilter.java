package ua.com.challenge.effects;

import android.content.res.Resources;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.renderscript.Script;
import ua.com.challenge.R;
import ua.com.challenge.filters.scripts.ScriptC_tiltshiftfilter;

/**
 * Created with IntelliJ IDEA.
 * User: cesaraguilar
 * Date: 12/4/12
 * Time: 3:02 PM
 */
public class TiltShiftGaussianBlurFilter extends GaussianBlurFilter {

    ScriptC_tiltshiftfilter mScript;
    public TiltShiftGaussianBlurFilter(RenderScript rs) {
        super(rs);
        mScript = new ScriptC_tiltshiftfilter(rs);
        init();
    }

    public TiltShiftGaussianBlurFilter(RenderScript rs, Resources resources, int id) {
        super(rs, resources, id);
        mScript = new ScriptC_tiltshiftfilter(rs, resources, R.raw.tiltshiftfilter);
        init();
    }

    public void init(){
        setBlurSize(2.0f);
        mScript.set_bottomFocusLevel(0.6f);
        mScript.set_direction(1);
        mScript.set_focusFallOffRate(0.2f);
        mScript.set_topFocusLevel(0.4f);
    }


    public void set_imageWidth(int width){
        super.set_imageWidth(width);
        mScript.set_imageWidth(width);
    }

    public void set_imageHeight(int height){
        super.set_imageHeight(height);
        mScript.set_imageHeight(height);
    }

    @Override
    public void invoke_filter(Script script,Allocation mInAllocation, Allocation mOutAllocation) {
        super.invoke_filter(script, mInAllocation, mOutAllocation);
        mScript.set_blurTexture(mOutAllocation);
        mScript.set_sharpTexture(mInAllocation);
        mScript.invoke_filter(mScript,mInAllocation, mOutAllocation);
    }

    public void set_topFocusLevel(float bottomLevel){
        mScript.set_topFocusLevel(bottomLevel);
    }
    public void set_focusFallOffRate(float bottomLevel){
        mScript.set_focusFallOffRate(bottomLevel);
    }
    public void set_direction(int bottomLevel){
        mScript.set_direction(bottomLevel);
    }
    public void set_bottomFocusLevel(float bottomLevel){
        mScript.set_bottomFocusLevel(bottomLevel);
    }
}
