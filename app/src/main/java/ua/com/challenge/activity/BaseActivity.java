package ua.com.challenge.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentId());

        initViews();
        setListeners();
    }

    protected abstract int getContentId();

    protected void setListeners() {
    }

    protected void initViews() {

    }
}
