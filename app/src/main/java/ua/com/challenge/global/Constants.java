package ua.com.challenge.global;

public class Constants {
    //camera request
    public final static int IMAGE_CAMERA_REQUEST = 1001;
    public final static int IMAGE_EXTERNAL_REQUEST = 1002;
    public final static int VIDEO_CAMERA_REQUEST = 1003;
    public final static int VIDEO_EXTERNAL_REQUEST = 1004;

    public final static int CROP_CIRCLE = 2001;
    public final static int CROP_ASPECT1_1 = 2002;
    public final static int CROP_RATIO_FREE = 2003;
}
