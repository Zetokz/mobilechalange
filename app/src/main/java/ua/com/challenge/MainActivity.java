package ua.com.challenge;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.isseiaoki.simplecropview.CropImageView;

import ua.com.challenge.activity.BaseActivity;
import ua.com.challenge.effects.FilterObject;
import ua.com.challenge.exception.PermissionSecurityException;
import ua.com.challenge.global.Constants;
import ua.com.challenge.utils.CropModeHelper;
import ua.com.challenge.utils.FileUtils;
import ua.com.challenge.utils.FilterUtils;
import ua.com.challenge.utils.ImageUtils;

public class MainActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, NavigationView.OnNavigationItemSelectedListener,
        SeekBar.OnSeekBarChangeListener {

    //region ContentChooser
    private CropImageView mImageToCrop;

    private Button mBtnRotate;
    private Button mBtnCrop;
    private Button mBtnChoosePhoto;

    private RelativeLayout mContentChooser;

    private Spinner mSpinnerCropType;
    //endregion ContentChooser

    //region MainActivity
    private ImageUtils.MediaChooseHelper mMediaChooser;
    private DrawerLayout drawer;
    private Uri mPickedImageUri;
    private boolean isChooserVisible = true;
    //endregion MainActivity

    //region EditPhoto
    private Bitmap mFilteredBitmap;
    private Bitmap mCroppedBitmap;

    private ImageView mCroppedImageView;
    private SeekBar mSeekBarChangeEffect;

    private EditText mEtRotationDegree;

    private FilterObject filterObject;
    private FilterUtils filterUtils;

    private NavigationView navigationView;

    private RelativeLayout mContentEditor;
    private Button mBtnSavePhoto;
    private Button mBtnResetPhoto;
    private Button mbtnChangeEffect;
    //endregion EditPhoto

    @Override
    protected void initViews() {
        mEtRotationDegree = (EditText) findViewById(R.id.etRotationDegree);
        mContentChooser = (RelativeLayout) findViewById(R.id.llContentChoosePhoto);
        mContentEditor = (RelativeLayout) findViewById(R.id.rlContentEditPhoto);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation);
        mBtnChoosePhoto = (Button) findViewById(R.id.btnChoseNewPhoto);
        mSpinnerCropType = (Spinner) findViewById(R.id.spinnerChangeCropMode);
        mBtnChoosePhoto = (Button) findViewById(R.id.btnChoseNewPhoto);
        mBtnCrop = (Button) findViewById(R.id.btnCrop);
        mBtnRotate = (Button) findViewById(R.id.btnRotate);
        mBtnSavePhoto = (Button) findViewById(R.id.btnSavePhoto);
        mBtnResetPhoto = (Button) findViewById(R.id.btnResetPhoto);
        mImageToCrop = (CropImageView) findViewById(R.id.cropImageView);
        mCroppedImageView = (ImageView) findViewById(R.id.croppedImageView);
        mSeekBarChangeEffect = (SeekBar) findViewById(R.id.sbChangeEffect);
        mbtnChangeEffect = (Button) findViewById(R.id.btnChangeEffect);
        super.initViews();
    }

    @Override
    protected void setListeners() {
        mBtnChoosePhoto.setOnClickListener(this);
        mSpinnerCropType.setOnItemSelectedListener(this);
        mBtnChoosePhoto.setOnClickListener(this);
        mBtnCrop.setOnClickListener(this);
        mBtnRotate.setOnClickListener(this);
        mBtnSavePhoto.setOnClickListener(this);
        mBtnResetPhoto.setOnClickListener(this);
        mSeekBarChangeEffect.setOnSeekBarChangeListener(this);
        mbtnChangeEffect.setOnClickListener(this);
        mSeekBarChangeEffect.setVisibility(View.GONE);

        filterObject = new FilterObject("Basic","basic",50,100,0,1);
        navigationView.setNavigationItemSelectedListener(this);
        super.setListeners();
    }

    @Override
    protected int getContentId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMediaChooser = ImageUtils.MediaChooseHelper.newInstance(Constants.IMAGE_CAMERA_REQUEST, Constants
                        .IMAGE_EXTERNAL_REQUEST, Constants.VIDEO_CAMERA_REQUEST, Constants.VIDEO_EXTERNAL_REQUEST, 0,
                savedInstanceState);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.crop_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerCropType.setAdapter(adapter);
        filterObject = new FilterObject("Basic", "basic", 50, 100, 0, 1);

    }

    @Override
    public void onClick(final View v) {
        try {
            switch (v.getId()) {
                case R.id.btnSavePhoto:
                    final String time = String.valueOf(System.currentTimeMillis());
                    FileUtils.createDir(Environment.getExternalStorageDirectory() + "/CoolPhoto");
                    final String resultFileName = Environment.getExternalStorageDirectory() + "/CoolPhoto/" + time.substring(0, 6) + ".png";
                    ImageUtils.saveToFile(resultFileName, mFilteredBitmap, Bitmap.CompressFormat.PNG, 90);
                    break;
                case R.id.btnChangeEffect:
                    drawer.openDrawer(GravityCompat.START);
                    break;
                case R.id.btnResetPhoto:
                    mCroppedImageView.setImageBitmap(mImageToCrop.getCroppedBitmap());

                    mCroppedBitmap = mImageToCrop.getCroppedBitmap();
                    mFilteredBitmap = mImageToCrop.getCroppedBitmap();

                    break;
                case R.id.btnRotate:
                    if (!TextUtils.isEmpty(mEtRotationDegree.getText())) {
                        mImageToCrop.setImageBitmap(ImageUtils.rotateBitmap(mCroppedBitmap, mEtRotationDegree.getText().toString()));
                    }
                    break;
                case R.id.btnCrop:
                    mCroppedBitmap = getSqureWithAlpha(mImageToCrop.getCroppedBitmap());
                    mFilteredBitmap = mCroppedBitmap;

                    filterUtils = new FilterUtils(this, mCroppedBitmap);
                    mCroppedImageView.setImageBitmap(mCroppedBitmap);

                    changeContentVisibility();
                    break;
                case R.id.btnChoseNewPhoto:
                    mMediaChooser.showImageChooser(this);
                    break;
            }
        } catch (PermissionSecurityException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.please_check_permission, Toast.LENGTH_LONG).show();
        }
    }

    private void changeContentVisibility() {
        if (isChooserVisible) {
            mContentChooser.setVisibility(View.GONE);
            mContentEditor.setVisibility(View.VISIBLE);
            isChooserVisible = false;
        } else {
            mContentChooser.setVisibility(View.VISIBLE);
            mContentEditor.setVisibility(View.GONE);
            isChooserVisible = true;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (mMediaChooser.onActivityResult(requestCode, resultCode, data)) {
            if (requestCode == Constants.IMAGE_CAMERA_REQUEST || requestCode == Constants.IMAGE_EXTERNAL_REQUEST) {
                mPickedImageUri = mMediaChooser.getPickedImageUri();
                Glide.with(this)
                        .load(mPickedImageUri)
                        .asBitmap()
                        .into(new BitmapImageViewTarget(mImageToCrop) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                mCroppedBitmap = resource;
                                ImageUtils.resetAngle();
                                ImageUtils.addPhotoToGallery(mPickedImageUri, getApplicationContext());
                                super.setResource(resource);
                            }
                        });
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final int id = item.getItemId();

//        mSeekBarChangeEffect.setMax(0);
//        mSeekBarChangeEffect.setProgress(100);

        switch (id) {
            case R.id.menu_item_effect_brightness:
                filterObject = new FilterObject("Brightness", "brightness", 100, 200, 100, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_contrast:
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                filterObject = new FilterObject("Contrast", "contrast", 200, 400, 100, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                break;

            case R.id.menu_item_effect_saturation:
                filterObject = new FilterObject("Saturation", "saturation", 100, 200, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_exposure:
                filterObject = new FilterObject("Exposure", "exposure", 1000, 2000, 1000, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_rgb:
                filterObject = new FilterObject("RGB", "rgb", 100, 200, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_hue:
                filterObject = new FilterObject("Hue", "hue", 90, 360, 0, 1);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_monochrome:
                filterObject = new FilterObject("Monochrome", "monochrome", 50, 100, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_gamma:
                filterObject = new FilterObject("Gamma", "gamma", 100, 300, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_sepia:
                filterObject = new FilterObject("Sepia", "sepia", 50, 100, 0, 1);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.GONE);

                break;

            case R.id.menu_item_effect_grayscale:
                filterObject = new FilterObject("Gray Scale", "grayscale", 50, 100, 0, 1);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.GONE);

                break;

            case R.id.menu_item_effect_invert_color:
                filterObject = new FilterObject("Invert Color", "invertcolor", 50, 100, 0, 1);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.GONE);

                break;

            case R.id.menu_item_effect_luminence_threshold:
                filterObject = new FilterObject("Luminance Threshold", "luminancethreshold", 50, 100, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_tone_curve:
                filterObject = new FilterObject("Tone Curve", "tonecurve", 50000, 100000, 0, 100000);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_gaussian_blur:
                filterObject = new FilterObject("Gaussian Blur", "gaussianblur", 50000, 400000, 0, 100000);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_selective_gaussian_blur:
                filterObject = new FilterObject("Selective Gaussian Blur", "gaussianselectiveblur", 18750, 75000, 0, 100000);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_item_effect_tilt_shift:
                filterObject = new FilterObject("TiltShift", "tiltshift", 30000, 60000, -20000, 100000);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);

                break;

            case R.id.menu_item_effect_tilt_shift_vertical:
                filterObject = new FilterObject("TiltShift Vertical", "tiltshiftvertical", 30000, 60000, -20000, 100000);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);

                break;

            case R.id.menu_item_effect_opacity:
                filterObject = new FilterObject("Opacity", "opacity", 100, 100, 0, 100);
                filterUtils.setOriginalBitmap(((BitmapDrawable) mCroppedImageView.getDrawable()).getBitmap());
                mSeekBarChangeEffect.setMax((int) filterObject.getMaxValue());
                mSeekBarChangeEffect.setProgress((int) filterObject.getDefaultValue());
                mSeekBarChangeEffect.setVisibility(View.VISIBLE);

                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
        switch (position) {
            case 0:
                CropModeHelper.circle(mImageToCrop);
                break;
            case 1:
                CropModeHelper.aspect1_1(mImageToCrop);
                break;
            case 2:
                CropModeHelper.ratioFree(mImageToCrop);
                break;
        }
    }

    @Override
    public void onNothingSelected(final AdapterView<?> parent) {}

    @Override
    public void onBackPressed() {
        if (!isChooserVisible) {
            changeContentVisibility();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        addEffect(seekBar, filterUtils);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        addEffect(seekBar, filterUtils);
    }

    public void addEffect(SeekBar seekBar, FilterUtils filterUtils) {
        float newI = seekBar.getProgress();
        newI = newI - filterObject.getMinusValue();
        float value = newI / filterObject.getDivisorValue();
        filterUtils.applyFilter(filterObject.getValue(), value);
        mCroppedImageView.setImageBitmap(filterUtils.getResultBitmap());
    }

    public void changeEffect(View view) {
    }

    public Bitmap getCroppedBitmap() {
        Bitmap bm = null;
        Drawable d = mImageToCrop.getDrawable();
        if (d != null && d instanceof BitmapDrawable) bm = ((BitmapDrawable) d).getBitmap();
        Bitmap source = bm;

        Bitmap cropped = Bitmap.createBitmap(
                source, 0, 0, mImageToCrop.getCroppedBitmap()
                        .getWidth(), mImageToCrop
                .getCroppedBitmap().getHeight(), null, false);
        return getSqureWithAlpha(cropped);
    }

    public Bitmap getSqureWithAlpha(Bitmap square) {
        if (square == null) return null;
        Bitmap output = Bitmap.createBitmap(square.getWidth(), square.getHeight(),
                Bitmap.Config.ARGB_8888);

        final Rect rect = new Rect(0, 0, square.getWidth(), square.getHeight());
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);

        canvas.drawRect(rect, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        canvas.drawBitmap(square, rect, rect, paint);
        return output;
    }

    private Bitmap adjustOpacity(Bitmap bitmap, int opacity)
    {
        Bitmap mutableBitmap = bitmap.isMutable()
                ? bitmap
                : bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        int colour = (opacity & 0xFF) << 24;
        canvas.drawColor(colour, PorterDuff.Mode.DST_IN);
        return mutableBitmap;
    }
}
